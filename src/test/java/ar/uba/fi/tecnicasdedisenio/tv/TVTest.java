package ar.uba.fi.tecnicasdedisenio.tv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/*
 * We are building a tv that can handle different operations from a remote control.
 * We need to allow the system to add new operations easily in the future.
 * 
 * Implement the code needed to make the tests pass.
 */
public class TVTest {
//     @Test
//     public void turnOnAndOff() {
//         List<Operation> operations = new ArrayList<>();
//         operations.add(new TurnOn("TurnOn"));
//         operations.add(new TurnOff("TurnOff"));

//         TV tv = new TV(operations);
//         tv.execute("TurnOn");

//         assertEquals(true, tv.isOn());
//     }

//    @Test
//     public void turnOnVolumeUpDownAndTurnOff() {
//         List<Operation> operations = new ArrayList<>();
//         operations.add(new TurnOn("TurnOn"));
//         operations.add(new TurnOff("TurnOff"));
//         operations.add(new VolumeUp("VolumeUp"));
//         operations.add(new VolumeDown("VolumeDown"));

//         TV tv = new TV(operations);
//         tv.execute("TurnOn");
//         tv.execute("VolumeUp");
//         tv.execute("VolumeUp");
//         tv.execute("VolumeUp");
//         tv.execute("VolumeDown");
//         tv.execute("TurnOff");

//         assertEquals(false, tv.isOn());
//         assertEquals(10, tv.getVolume());
//     }

//     @Test
//     public void turnOnChannelUpDownAndTurnOff() {
//         List<Operation> operations = new ArrayList<>();
//         operations.add(new TurnOn("TurnOn"));
//         operations.add(new TurnOff("TurnOff"));
//         operations.add(new VolumeUp("VolumeUp"));
//         operations.add(new VolumeDown("VolumeDown"));
//         operations.add(new ChannelUp("ChannelUp"));
//         operations.add(new ChannelDown("ChannelDown"));

//         TV tv = new TV(operations);
//         tv.execute("TurnOn");
//         tv.execute("ChannelUp");
//         tv.execute("ChannelUp");
//         tv.execute("ChannelUp");
//         tv.execute("ChannelUp");
//         tv.execute("ChannelUp");
//         tv.execute("VolumeUp");
//         tv.execute("VolumeUp");
//         tv.execute("TurnOff");

//         assertEquals(false, tv.isOn());
//         assertEquals(5, tv.getChannel());
//         assertEquals(10, tv.getVolume());
//     }
}