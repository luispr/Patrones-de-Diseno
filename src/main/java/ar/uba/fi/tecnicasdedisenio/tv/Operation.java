package ar.uba.fi.tecnicasdedisenio.tv;

public interface Operation {
    public String getName();
    public void execute(final TV tv);
}
