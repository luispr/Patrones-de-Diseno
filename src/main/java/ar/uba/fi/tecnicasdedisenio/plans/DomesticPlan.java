package ar.uba.fi.tecnicasdedisenio.plans;

public class DomesticPlan extends Plan {
    @Override
    protected double getRate() {
        return 5.0;
    }
}
