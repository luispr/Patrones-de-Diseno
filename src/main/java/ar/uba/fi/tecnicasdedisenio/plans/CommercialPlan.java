package ar.uba.fi.tecnicasdedisenio.plans;

public class CommercialPlan extends Plan {
    @Override
    protected double getRate() {
        return 7.5;
    }
}
