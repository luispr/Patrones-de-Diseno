package ar.uba.fi.tecnicasdedisenio.plans;

import java.util.HashMap;

public class PlanCreator {

    private HashMap<String, Plan> planMap = new HashMap<String, Plan>();

    public PlanCreator () {
        planMap.put("Domestic", new DomesticPlan());
        planMap.put("Commercial", new CommercialPlan());
        planMap.put("Institutional", new InstitutionalPlan());
    }
    public Plan get(String planType) {
        if (planType == null) {
            throw new IllegalArgumentException("Plan type can't be null");
        }

        if (this.planMap.get(planType) == null) {
            throw new IllegalArgumentException("Plan type not found");
        }

        return this.planMap.get(planType);
    }
}
