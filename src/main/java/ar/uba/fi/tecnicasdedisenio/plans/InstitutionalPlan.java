package ar.uba.fi.tecnicasdedisenio.plans;

public class InstitutionalPlan extends Plan {
    @Override
    protected double getRate() {
        return 5.5;
    }
}
