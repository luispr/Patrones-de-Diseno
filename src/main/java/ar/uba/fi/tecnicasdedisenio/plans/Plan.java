package ar.uba.fi.tecnicasdedisenio.plans;

abstract class Plan {
  protected abstract double getRate();

  public double calculateBill(int units) {
    return units * this.getRate();
  }


  public int calculuateBill(int units) {
    return (int) this.calculateBill(units);
  }
}