package ar.uba.fi.tecnicasdedisenio.shapes;

public class Square implements Shape {

    @Override
    public String draw() {
        return "Square";
    }
    
}
