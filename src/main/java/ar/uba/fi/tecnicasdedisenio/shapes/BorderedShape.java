package ar.uba.fi.tecnicasdedisenio.shapes;

public class BorderedShape implements Shape {

    private Shape shape;
    private String color;
    public BorderedShape(Shape shape, String red) {
        this.shape = shape;
        this.color = red;
    }


    public String draw() {
        return this.color + " " + this.shape.draw();
    }
}
