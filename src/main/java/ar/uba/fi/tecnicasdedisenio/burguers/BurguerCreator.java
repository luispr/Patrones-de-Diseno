package ar.uba.fi.tecnicasdedisenio.burguers;

import java.util.ArrayList;
import java.util.List;

public class BurguerCreator {

    private List<String> ingredients = new ArrayList<>();
    public void addMeet() {
        this.ingredients.add("Meet");
    }

    public Burguer build() {
        return new Burguer(this.ingredients);
    }

    public void addLetuce() {
        this.ingredients.add("Letuce");
    }

    public void addTomato() {
        this.ingredients.add("Tomato");
    }

    public void addCucumber() {
        this.ingredients.add("Cucumber");
    }

    public void addCheeken() {
        this.ingredients.add("Meet");
    }
}
