# Ejercicios de Patrones de Diseño

En esta semana hicimos TDD para ir descubriendo algunos patrones de diseño

- Patrones de diseño vistos:
  - [Builder Pattern](./src/main/java/ar/uba/fi/tecnicasdedisenio/burguers/)
  - [Factory Pattern](./src/main/java/ar/uba/fi/tecnicasdedisenio/plans/)
  - [Flyweight](./src/main/java/ar/uba/fi/tecnicasdedisenio/shapes/)
  - [Decorator](./src/main/java/ar/uba/fi/tecnicasdedisenio/tv/)

# Notion

[Notion](https://mis-notas.notion.site/Semana-13-04c54176b4d54fa2a86d1aff75a02971?pvs=4)
